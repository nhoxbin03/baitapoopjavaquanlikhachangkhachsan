package danhsachquanlikhachhangcuakhachsan;
import java.util.ArrayList;
import java.util.List;


public class khachsan {
	 public List<Nguoi> nguois;  // tao list nguois

	    public khachsan( ) {
	        nguois = new ArrayList<>(); // tao lkist nguois
	    }

	    public void add(Nguoi nguoi) { // add o zo list
	    	
	        this.nguois.add(nguoi);
	    }

	    public boolean delete(String id) {
	    	 Nguoi  nguoi = this.nguois.stream().filter( o ->  o.getId().equals(id)).findFirst().orElse(null);
	        if ( nguoi == null) {
	            return false;
	        } else {
	            this.nguois.remove( nguoi);
	            return true;
	        }
	    }

	    public int calculator(String id) {
	    	 Nguoi  nguoi = this.nguois.stream().filter( o ->  o.getId().equals(id)).findFirst().orElse(null);
	        if ( nguoi == null) {
	            return 0;
	        }
	        return  nguoi.getRoom().getPrice() *  nguoi.getNumberRent();
	    }

	    public void show() {
	        this.nguois.forEach( o -> System.out.println( o.toString()));
	    }

}
